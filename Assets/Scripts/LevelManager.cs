﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace Arkanoid
{
    public class LevelManager : MonoBehaviour {

        public TextAsset levelsXML;
        public GameObject brickPrefab;

        public int maxLevels { get; private set; }
        int[][][] allLevels;
        public int currentLevel;

        private static LevelManager _instance = null;
        public static LevelManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<LevelManager>();
                }
                return _instance;
            }
        }

        void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
            }
            DontDestroyOnLoad(gameObject);
        }

        void Start() {
            Parse();
        }

        void Parse()
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(levelsXML.text);

            XmlNode data = null;
            foreach (XmlNode node in xml.ChildNodes)
            {
                if (node.Name.Equals("data"))
                {
                    data = node;
                    break;
                }
            }
            allLevels = new int[data.ChildNodes.Count][][];
            maxLevels = allLevels.Length;
            foreach (XmlNode levelNode in data.ChildNodes)
            {
                if (levelNode.Name.Equals("level"))
                {
                    // так как массив с 0, а нумерация уровней с 1, то отнимаем единицу
                    int index = int.Parse(levelNode.Attributes.GetNamedItem("id").Value) - 1;

                    allLevels[index] = new int[levelNode.ChildNodes.Count][];

                    string[] rows = new string[levelNode.ChildNodes.Count];
                    for (int i = 0; i < rows.Length; i++)
                    {
                        rows[i] = levelNode.ChildNodes[i].Attributes.GetNamedItem("bricks").Value;
                        string[] bricks = rows[i].Split(',');
                        allLevels[index][i] = new int[bricks.Length];
                        for (int j = 0; j < bricks.Length; j++)
                        {
                            allLevels[index][i][j] = int.Parse(bricks[j]);
                        }
                    }
                }
            }
        }

        public Brick[] BuildLevel(int levelNumber, Transform parent, Rect globalRect)
        {
            List<Brick> result = new List<Brick>();
            float areaWidth = globalRect.width;
            Bounds brickBounds = brickPrefab.GetComponent<Renderer>().bounds;
            float brickWidth = brickBounds.size.x;

            int[][] levelSettings = allLevels[levelNumber - 1]; // ибо нумерация уровней с 1, а массив с 0
            // самый длинный ряд
            int maxRowCount = 0;
            foreach (int[] row in levelSettings)
            {
                if (row.Length > maxRowCount)
                {
                    maxRowCount = row.Length;
                }
            }

            // если самый длинный ряд не вместится в ширину поля, то его надо ужать.
            float scale = 1;
            if (brickWidth * maxRowCount > areaWidth)
            {
                scale = areaWidth / (brickWidth * maxRowCount);
            }

            // теперь исходя из новой ширины кирпичей начинаем строить уровень начиная с верхнего ряда
            brickWidth *= scale;
            float brickHeight = brickBounds.size.y * scale;
            for (int i = 0; i < levelSettings.Length; i++)
            {
                float y = globalRect.yMax - brickHeight * (i + 0.5f);
                float xmin = globalRect.xMin + 0.5f * (globalRect.width - brickWidth * levelSettings[i].Length);
                for (int j = 0; j < levelSettings[i].Length; j++)
                {
                    if (levelSettings[i][j] != 0)
                    {
                        GameObject newBrick = Instantiate(brickPrefab,
    new Vector3(xmin + brickWidth * (j + 0.5f), y, 0f), Quaternion.identity) as GameObject;
                        newBrick.transform.localScale = Vector3.one * scale;
                        result.Add(newBrick.GetComponent<Brick>());
                        newBrick.transform.SetParent(parent, true);
                    }
                }
            }

            return result.ToArray();
        }
    }
}
