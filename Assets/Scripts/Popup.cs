﻿using UnityEngine;
using System.Collections;

namespace Arkanoid
{
    public class Popup : MonoBehaviour
    {
        public void Open()
        {
            gameObject.SetActive(true);
        }

        public void Close()
        {
            gameObject.SetActive(false);
        }
    }
}
