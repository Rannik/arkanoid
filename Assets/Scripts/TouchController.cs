﻿using UnityEngine;
using System.Collections;

namespace Arkanoid
{
    public class TouchController : MonoBehaviour
    {

        public System.Action<Vector2> OnMouseDown;

        // Update is called once per frame
        void Update()
        {
            // проверить, не находится ли палец над гуи-элементом
            // Если нет, то выдать ивент
            if (Input.GetMouseButton(0) && OnMouseDown != null &&
                !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
            {
                OnMouseDown(Input.mousePosition);
            }
        }
    }
}
