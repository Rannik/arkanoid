﻿using UnityEngine;
using System.Collections;

namespace Arkanoid
{
    public class ScoreController
    {
        public int currentScore { get; private set; }
        const string recordKeyPrefs = "record";
        public int record { get; private set; }
        public System.Action<int> OnChangeCurrentScore;

        private static ScoreController _instance = null;
        public static ScoreController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ScoreController();
                }
                return _instance;
            }
        }

        private ScoreController()
        {
            record = PlayerPrefs.GetInt(recordKeyPrefs, 0);
            ResetCurrentScore();
        }

        public void ResetCurrentScore()
        {
            currentScore = 0;
            if (OnChangeCurrentScore != null)
            {
                OnChangeCurrentScore(currentScore);
            }
        }

        public void AddScore(int add = 1)
        {
            currentScore += add;
            if (OnChangeCurrentScore != null)
            {
                OnChangeCurrentScore(currentScore);
            }
            CheckRecord();
        }

        void CheckRecord()
        {
            if (currentScore > record)
            {
                WriteNewRecord();
            }
        }

        void WriteNewRecord()
        {
            record = currentScore;
            PlayerPrefs.SetInt(recordKeyPrefs, record);
            PlayerPrefs.Save();
        }
    }
}
