﻿using UnityEngine;
using System.Collections;

namespace Arkanoid
{
    public class Brick : MonoBehaviour
    {

        public System.Action<Brick> OnDestroyBrick;

        void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.GetComponent<Ball>() != null)
            {
                DestroyBrick();
            }
        }

        void DestroyBrick()
        {
            ScoreController.Instance.AddScore();
            if (OnDestroyBrick != null)
            {
                OnDestroyBrick(this);
            }

            Destroy(gameObject);
        }
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}