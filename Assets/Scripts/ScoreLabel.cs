﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Arkanoid
{
    [RequireComponent(typeof(Text))]
    public class ScoreLabel : MonoBehaviour
    {
        Text thisText;

        void Awake()
        {
            thisText = GetComponent<Text>();
        }

        void Start()
        {
            ChangeScore(ScoreController.Instance.currentScore);
            ScoreController.Instance.OnChangeCurrentScore += ChangeScore;
        }

        void ChangeScore(int newValue)
        {
            thisText.text = newValue.ToString();
        }

        void OnDestroy()
        {
            ScoreController.Instance.OnChangeCurrentScore -= ChangeScore;
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}