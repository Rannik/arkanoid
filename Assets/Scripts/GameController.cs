﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Arkanoid
{
    public class GameController : MonoBehaviour
    {
        public Popup PausePopup;
        public Popup WinPopup;
        public GameObject nextLevelButton;
        public Popup LosePopup;

        public UnityEngine.UI.Text levelNumber;

        public Platform platform;
        public TouchController touchController;
        public Transform gameField;

        public BoxCollider2D leftWall;
        public BoxCollider2D rightWall;
        public BoxCollider2D topWall;
        public BoxCollider2D bottomWall;

        public BottomBorder bottom;

        public GameObject bonusPrefab;

        public List<Ball> allBalls;
        public float normalBallSpeed;
        public float fastBallSpeed;
        public float timeForBallSpeedBonus = 15f;
        float ballSpeedTimer = Mathf.Infinity;
        bool isBallSpeedTimerWorks = false;

        Camera mainCamera;

        void RestartBallSpeedTimer()
        {
            ballSpeedTimer = timeForBallSpeedBonus;
            isBallSpeedTimerWorks = true;
        }

        public List<Brick> allBricks;

        void Start()
        {
            mainCamera = Camera.main;

            levelNumber.text = LevelManager.Instance.currentLevel.ToString();

            BuildField();

            float platformHalfWidth = platform.GetComponent<Renderer>().bounds.extents.x;
            float cameraHalfWidth = mainCamera.aspect * mainCamera.orthographicSize;
            platform.Init(-cameraHalfWidth + platformHalfWidth, cameraHalfWidth - platformHalfWidth, -3);

            touchController.OnMouseDown += MovePlatrorm;

            foreach(Brick brick in allBricks)
            {
                brick.OnDestroyBrick += BrickDestroyed;
            }

            bottom.OnBallTouched += DestroyBall;


            foreach (Ball ball in allBalls)
            {
                ball.StartMove(Vector2.one, normalBallSpeed);
            }

            Time.timeScale = 1f;
        }

        public void ShowPause()
        {
            PausePopup.Open();
            Time.timeScale = 0f;
        }

        public void ClosePause()
        {
            PausePopup.Close();
            Time.timeScale = 1f;
        }

        public void Win()
        {
            WinPopup.Open();
            if (LevelManager.Instance.currentLevel == LevelManager.Instance.maxLevels)
            {
                nextLevelButton.SetActive(false);
            }
            Time.timeScale = 0f;
        }

        public void Lose()
        {
            LosePopup.Open();
            Time.timeScale = 0f;
        }

        public void LoadMenu()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
        }

        public void NextLevel()
        {
            LevelManager.Instance.currentLevel++;
            UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
        }

        void BuildField()
        {
            // определить границы и поместить туда стены
            float cameraHalfWidth = mainCamera.aspect * mainCamera.orthographicSize;
            leftWall.transform.position = new Vector3(-cameraHalfWidth - leftWall.size.x * 0.5f, 0f, 0f);
            rightWall.transform.position = new Vector3(cameraHalfWidth + rightWall.size.x * 0.5f, 0f, 0f);
            bottomWall.transform.position = new Vector3(0, -mainCamera.orthographicSize - bottomWall.size.y * 0.5f);
            topWall.transform.position = new Vector3(0, mainCamera.orthographicSize - 1.5f + topWall.size.y * 0.5f);

            // определить границы поля:
            Rect fieldRect = new Rect(-cameraHalfWidth, -mainCamera.orthographicSize,
                cameraHalfWidth * 2, topWall.bounds.min.y - bottomWall.bounds.max.y - 1);
            allBricks = new List<Brick>(LevelManager.Instance.BuildLevel(LevelManager.Instance.currentLevel, 
                gameField, fieldRect));

        }

        void MovePlatrorm(Vector2 newPosition)
        {
            platform.MoveTo(mainCamera.ScreenToWorldPoint(newPosition).x);
        }

        void BrickDestroyed(Brick sender)
        {
            if (IsCreateBonus())
            {
                CreateRandomBonus(sender.transform.position);
            }

            allBricks.Remove(sender);
            if (allBricks.Count == 0)
            {
                foreach(Ball ball in allBalls)
                {
                    ball.ChangeSpeed(0);
                }
                Win();
            }
        }

        bool IsCreateBonus()
        {
            return Random.Range(0f, 1f) < 0.5f;
        }

        void CreateRandomBonus(Vector3 position)
        {
            BonusType[] bonusTypes = (BonusType[])System.Enum.GetValues(typeof(BonusType));

            GameObject newBonus = Instantiate(bonusPrefab, position, Quaternion.identity) as GameObject;
            Bonus bonus = newBonus.GetComponent<Bonus>();
            bonus.Init(bonusTypes[Random.Range(0, bonusTypes.Length)]);
            bonus.OnBonusCatched += BonusCatched;
        }

        void BonusCatched(BonusType bonusType)
        {
            switch(bonusType)
            {
                case BonusType.BigPlatform:
                    platform.MakeBig();
                    break;
                case BonusType.SmallPlatform:
                    platform.MakeSmall();
                    break;
                case BonusType.SpeedUp:
                    SpeedUpBalls();
                    break;
                case BonusType.Clone:
                    CloneBalls();
                    break;
            }
        }

        void SpeedUpBalls()
        {
            foreach (Ball ball in allBalls)
            {
                ball.ChangeSpeed(fastBallSpeed);
            }
            RestartBallSpeedTimer();
        }

        void NormalSpeed()
        {
            foreach(Ball ball in allBalls)
            {
                ball.ChangeSpeed(normalBallSpeed);
            }
            isBallSpeedTimerWorks = false;
        }

        void CloneBalls()
        {
            Ball cloneBall = allBalls[0];

            GameObject newBallObject = Instantiate(cloneBall.gameObject, 
                cloneBall.transform.position, Quaternion.identity) as GameObject;
            Ball newBall = newBallObject.GetComponent<Ball>();
            Vector3 newDirection = Quaternion.AngleAxis(120f, Vector3.forward) * cloneBall.direction;
            newBall.StartMove(newDirection, cloneBall.speed);
            allBalls.Add(newBall);

            newBallObject = Instantiate(cloneBall.gameObject, cloneBall.transform.position,
                Quaternion.identity) as GameObject;
            newBall = newBallObject.GetComponent<Ball>();
            newDirection = Quaternion.AngleAxis(-120f, Vector3.forward) * cloneBall.direction;
            newBall.StartMove(newDirection, cloneBall.speed);
            allBalls.Add(newBall);
        }

        void DestroyBall(Ball ball)
        {
            allBalls.Remove(ball);
            Destroy(ball.gameObject);
            if (allBalls.Count == 0)
            {
                Lose();
            }
        }

        void Update()
        {
            if (isBallSpeedTimerWorks)
            {
                ballSpeedTimer -= Time.deltaTime;
                if (ballSpeedTimer <= 0)
                {
                    NormalSpeed();
                }
            }
        }
    }
}
