﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Arkanoid
{
    public class MenuController : MonoBehaviour
    {
        public Text recordText;

        void Start()
        {
            recordText.text = ScoreController.Instance.record.ToString();
        }
        public void StartGame()
        {
            LevelManager.Instance.currentLevel = 1;
            ScoreController.Instance.ResetCurrentScore();
            UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
        }
    }
}