﻿using UnityEngine;
using System.Collections;

namespace Arkanoid
{
    public class Obstacle : MonoBehaviour
    {

        void OnCollisionEnter2D(Collision2D collision)
        {
            Ball ball = collision.gameObject.GetComponent<Ball>();
            if (ball != null)
            {
                ball.ChangeDirection(Vector2.Reflect(ball.direction, collision.contacts[0].normal));
            }
        }
    }
}
