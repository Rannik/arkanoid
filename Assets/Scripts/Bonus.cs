﻿using UnityEngine;
using System.Collections;

namespace Arkanoid
{
    public enum BonusType
    {
        BigPlatform,
        SmallPlatform,
        SpeedUp,
        Clone
    }

    public class Bonus : MonoBehaviour
    {
        float speed = 2;
        BonusType bonusType;
        public Sprite BigPlatformSprite;
        public Sprite SmallPlatformSprite;
        public Sprite SpeedUpSprite;
        public Sprite CloneSprite;

        public event System.Action<BonusType> OnBonusCatched;

        public void Init(BonusType bonusType)
        {
            this.bonusType = bonusType;
            SpriteRenderer thisRenderer = GetComponent<SpriteRenderer>();
            switch(bonusType)
            {
                case BonusType.BigPlatform:
                    thisRenderer.sprite = BigPlatformSprite;
                    break;
                case BonusType.SmallPlatform:
                    thisRenderer.sprite = SmallPlatformSprite;
                    break;
                case BonusType.SpeedUp:
                    thisRenderer.sprite = SpeedUpSprite;
                    break;
                case BonusType.Clone:
                    thisRenderer.sprite = CloneSprite;
                    break;
            }
        }

        void Start()
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.down * speed;
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            if(collision.gameObject.GetComponent<Platform>() != null)
            {
                if (OnBonusCatched != null)
                {
                    OnBonusCatched(bonusType);
                }
                Destroy(gameObject);
            }
        }

    }
}
