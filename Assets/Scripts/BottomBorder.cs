﻿using UnityEngine;
using System.Collections;

namespace Arkanoid
{
    public class BottomBorder : MonoBehaviour {

        public System.Action<Ball> OnBallTouched;

        void OnCollisionEnter2D(Collision2D collision)
        {
            Ball ball = collision.gameObject.GetComponent<Ball>();
            if (OnBallTouched != null && ball != null)
            {
                OnBallTouched(ball);
            }

            if (collision.gameObject.GetComponent<Bonus>() != null)
            {
                Destroy(collision.gameObject);
            }
        }
        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }
    }
}