﻿using UnityEngine;
using System.Collections;

namespace Arkanoid
{
    public class Platform : MonoBehaviour
    {
        const float speed = 15;

        float min = Mathf.NegativeInfinity;
        float max = Mathf.Infinity;

        Vector3 position;

        const float timeToNormalSize = 15f;
        float normalSizeTimer = Mathf.Infinity;
        bool isTimerWorks = false;

        void RestartTimer()
        {
            normalSizeTimer = timeToNormalSize;
            isTimerWorks = true;
        }

        /** Делаем платформу большой. Если она уже была большая, просто обновляем таймер
        */
        public void MakeBig()
        {
            transform.localScale = new Vector3(1.25f, 1f, 1f);
            RestartTimer();
        }

        /** Делаем платформу маленькой.
        */
        public void MakeSmall()
        {
            transform.localScale = new Vector3(0.75f, 1f, 1f);
            RestartTimer();
        }

        public void MakeNormal()
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
            isTimerWorks = false;
        }

        public void Init(float minX, float maxX, float y)
        {
            this.min = minX;
            this.max = maxX;
            position = new Vector3(0, y, 0);
            transform.position = position;
        }

        public void MoveTo(float newPositionX)
        {
            newPositionX = Mathf.Clamp(newPositionX, min, max);
            float delta = newPositionX - transform.position.x;
            if (Mathf.Abs(delta) < speed * Time.deltaTime)
            {
                position.x = newPositionX;
            }
            else
            {
                position.x += Mathf.Sign(delta) * speed * Time.deltaTime;
            }

            transform.position = position;
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            Ball ball = collision.gameObject.GetComponent<Ball>();
            if (ball != null)
            {
                Vector2 normal = collision.contacts[0].normal.normalized;
                if (normal.y < 0)
                {
                    normal *= -1;
                }
                normal += 0.5f * (collision.contacts[0].point - (Vector2)transform.position).normalized;
                ball.ChangeDirection(Vector2.Reflect(ball.direction, normal));
            }
        }

        void Update()
        {
            if (isTimerWorks)
            {
                normalSizeTimer -= Time.deltaTime;
                if (normalSizeTimer <= 0)
                {
                    MakeNormal();
                }
            }
        }
    }
}
