﻿using UnityEngine;
using System.Collections;

namespace Arkanoid
{
    public class Ball : MonoBehaviour
    {
        public Vector2 direction { get; private set; }
        Rigidbody2D thisRigidbody;
        public float speed { get; private set; }

        void Awake()
        {
            thisRigidbody = GetComponent<Rigidbody2D>();
        }

        public void StartMove(Vector2 direction, float speed)
        {
            this.direction = direction.normalized;
            this.speed = speed;

          //  UpdateVelocity();
        }

        /// <summary>
        ///   Смена скорости
        /// </summary>
        /// <param name="newSpeed"></param>
        public void ChangeSpeed(float newSpeed)
        {
            this.speed = newSpeed;

          //  UpdateVelocity();
        }

        /// Смена направления движения. Чаще всего при отскоке.
        public void ChangeDirection(Vector2 newDirection)
        {
            direction = newDirection.normalized;

           // UpdateVelocity();
        }

        void UpdateVelocity()
        {
            thisRigidbody.velocity = direction * speed;
        }

        void Move()
        {
            transform.Translate(direction * (speed * Time.deltaTime), Space.World);
        }

        // Update is called once per frame
        void Update()
        {
             Move();
        }
    }
}
